import unittest
import cangjie


class CommonFuncTestCase(unittest.TestCase):
    def test_get_version(self):
        self.assertNotEqual(cangjie.libcangjie_version(), "")
        self.assertNotEqual(cangjie.libcangjie_version(), "0.0.0")
        self.assertRegex(cangjie.libcangjie_version(), r"\d+\.\d+\.\d+")

    def test_check_version_gte(self):
        version_string = cangjie.libcangjie_version()

        # Remove any pre-release tags
        semver_string = version_string.split("-")[0]

        # Split the version string into major, minor, and patch
        (major, minor, patch) = semver_string.split(".")

        # Basic case
        self.assertTrue(
            cangjie.libcangjie_check_version_gte(int(major), int(minor), int(patch))
        )

        # Simple higher version checks
        self.assertTrue(
            cangjie.libcangjie_check_version_gte(
                int(major) + 1, int(minor) - 1, int(patch) - 1
            )
        )
        self.assertTrue(
            cangjie.libcangjie_check_version_gte(
                int(major), int(minor) + 1, int(patch) - 1
            )
        )
        self.assertTrue(
            cangjie.libcangjie_check_version_gte(int(major), int(minor), int(patch) + 1)
        )

        # Simple lower version checks
        self.assertFalse(
            cangjie.libcangjie_check_version_gte(
                int(major) - 1, int(minor) + 1, int(patch) + 1
            )
        )
        self.assertFalse(
            cangjie.libcangjie_check_version_gte(
                int(major), int(minor) - 1, int(patch) + 1
            )
        )
        self.assertFalse(
            cangjie.libcangjie_check_version_gte(int(major), int(minor), int(patch) - 1)
        )
